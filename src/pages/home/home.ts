import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { BookPage } from '../book/book';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  service: Array <any> = [];
  i
  constructor(public navCtrl: NavController, private alertController:AlertController) {
    this.show();
  }
  show(){
    this.service = [
      {
        "name": "Small",
        "wash": "156.00 pesos",
        "shine": "300.00 pesos"
      },
      {
        "name": "Medium",
        "wash": "170.00 pesos",
        "shine": "400.00 pesos"
      },
      {
        "name": "Large",
        "wash": "190.00 pesos",
        "shine": "500.00 pesos"
      }

    ]
  }
  Service(status){
    if(status == "Small"){
      let alert = this.alertController.create({
        title: "Small",
        message:"Wash: 150-170 pesos <br> Shine: 300-500 pesos",
        buttons:[
          {
            text:"cancel"
          },
          {
            text: "okay",
            handler: () => {
              let data = {
                  name: "small",
                  wash: "150-170 pesos",
                  shine: "300-500 pesos",
                  mat: "55-75 pesos",
                  leather: "300-500 pesos",
                  interior: "50-100 pesos"
                }
                this.navCtrl.push(BookPage,data);
              
            }
          }
        ]
      });
      alert.present();
    }
    if(status == "Medium"){
      let alert = this.alertController.create({
        title: "Medium",
        message:"Wash: 170-190 pesos <br> Shine: 400-600 pesos",
        buttons:[
          {
            text:"cancel"
          },
          {
            text: "okay",
            handler: () => {
              let data = {
                  name: "medium",
                  wash: "170-190 pesos",
                  shine: "400-600 pesos",
                  mat: "85-105 pesos",
                  leather: "400-600 pesos",
                  interior: "60-90 pesos"
                }
                this.navCtrl.push(BookPage,data);
              
            }
          }
        ]
      });
      alert.present();
    }
    if(status == "Large"){
      let alert = this.alertController.create({
        title: "Large",
        message:"Wash: 190-210 pesos <br> Shine: 500-700 pesos",
        buttons:[
          {
            text:"cancel"
          },
          {
            text: "okay",
            handler: () => {
              let data = {
                  name: "large",
                  wash: "190-210 pesos",
                  shine: "500-700 pesos",
                  mat: "110-120 pesos",
                  leather: "500-600 pesos",
                  interior: "70-100 pesos"
                }
                this.navCtrl.push(BookPage,data);
              
            }
          }
        ]
      });
      alert.present();
    }
  }

}
