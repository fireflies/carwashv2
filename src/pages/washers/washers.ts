import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { LocationPage } from '../location/location';

/**
 * Generated class for the WashersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-washers',
  templateUrl: 'washers.html',
})
export class WashersPage {
  responseTxt
  constructor(public navCtrl: NavController, public navParams: NavParams, private alertController:AlertController) {
    this.showTable();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WashersPage');
  }
  showTable(){
    this.responseTxt = [
        {
          "name": "John Doe",
          "address": "Talamban Cebu City",
          "status": "available"
        },
        {
          "name": "Redicc Mass",
          "address": "Banilad Cebu City",
          "status": "Booked"
        },
        {
          "name": "Johnny nis",
          "address": "Liloan Cebu City",
          "status": "Booked"
        },
        {
          "name": "Pedro",
          "address": "Minglanilla Cebu City",
          "status": "available"
        }
    ]
  }
  goToOtherPage(name,address,status){
    console.log(name, status), address;
    if(status == "Booked"){
      let alert = this.alertController.create({
        title: "Opss!!",
        message:"This washer is currently unavailable please book at another time.",
        buttons:[
          {
            text:"Okay"
          }]
      });
      alert.present();
    }else{
      let data ={
        names: name,
        stat: status,
        add: address
      } 
      this.navCtrl.push(LocationPage, data);    
    }
  
  }
}
