import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WashersPage } from './washers';

@NgModule({
  declarations: [
    WashersPage,
  ],
  imports: [
    IonicPageModule.forChild(WashersPage),
  ],
})
export class WashersPageModule {}
